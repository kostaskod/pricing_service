<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('prices_model');
		$this->load->helper('url_helper');
	}	 
	
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');	

		$this->form_validation->set_rules('amount', 'Amount', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('home');
		}
		else
		{
			$amount = $this->input->post('amount');
			$this->prices_model->set_prices($amount);
			$data['db_row'] = $this->prices_model->get_prices($amount);
			$this->load->view('templates/header');
			$this->load->view('success', $data);
			$this->load->view('templates/footer');	
		}
	}
}
