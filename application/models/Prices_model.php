<?php
	class Prices_model extends CI_Model {

		public function __construct()
		{
				$this->load->database();
		}
		
		public function set_prices($amount)
		{
			$vat = $amount - ($amount/1.24);
			$net = $amount - $vat;
			
			$data = array(
				'amount' => $amount,
				'vat' => $vat,
				'net' => $net
			);

			return $this->db->insert('invoices', $data);
		}
		
		public function get_prices($amount)
		{
			$query = $this->db->get_where('invoices', array('amount' => $amount));
			return $query->row_array();
		}
		
	}
?>