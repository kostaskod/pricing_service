<table>
  <tr>
    <th colspan="2">INPUT</th>
  </tr>
  <tr>
    <td>INVOICE AMOUNT</td>
    <td><?php echo $db_row['amount']; ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
</table> 
<br/>
<br/>
<table>
  <tr>
    <th colspan="2">OUTPUT</th>
  </tr>
  <tr>
    <td>INVOICE AMOUNT</td>
    <td><?php echo $db_row['amount']; ?></td>
  </tr>
  <tr>
    <td>VAT</td>
    <td><?php echo $db_row['vat']; ?></td>
  </tr>
  <tr>
    <td>NET</td>
    <td><?php echo $db_row['net']; ?></td>
  </tr>
</table> 